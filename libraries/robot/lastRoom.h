#ifndef LASTROOM_H_INCLUDED
#define LASTROOM_H_INCLUDED

#include <Move.h>
#include <Sensor.h>
#include <ReadNano.h>
#include <ReadPi.h>


class LastRoom{
  private:
    Move Movements;
    sensor Sensors;
    ReadNano Imu;
    ReadPi Camera;
  public:
    bool Compare(int tolerance);
    void TakeBall();
    void ReturnHome(bool pos);
    void init(int pinA_1,int pinA_2,int pinB_1,int pinB_2,int maxspeed);
};

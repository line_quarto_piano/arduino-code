#ifndef SENSOR_H
#define SENSOR_H
#include "Arduino.h"
#include <VL53L0X.h>
#include <string.h>

class sensor{
  private:
    VL53L0X *ToF;
    const int precision=5; //To define
    void SelectToF(String position);
    void TcaSelect(uint8_t i);
  public:
    void init();
    int ReadToFmm(String position);
    void SetToFDistanceReading(int distance,String position);
};
#endif

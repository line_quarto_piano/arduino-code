#include <ReadNANO.h>
#include <SoftwareSerial.h>

ReadNANO::ReadNANO(int rx,int tx){
  SoftwareSerial *port=new SoftwareSerial(rx,tx);
  port->begin(115200);
  this->serial=port;
  //Serial.println("Partenza");
}
int ReadNANO::readNANO(int &o,int &v) {
  //Serial.println("Lettura");
  serial->listen();
  char chr;
  while (serial->available()){
    chr=serial->read();}                          //Clean the buffer
  String str="";
  while(true) {
    chr=serial->read();
    if (chr=='#')                             //Search # character
      break;
  }
  while(chr!=',') {                        //Read chars and add them to a string while a comma isn't found
    if(serial->available()) {
      chr=serial->read();
      str+=chr;
    }
  }
  o=str.toInt();                        //Convert the string to an int
  o+=180;                               //Add to the int 180 because raw data are mapped in -180,+180
  str="";
  while(chr!='#') {
    if(serial->available()) {               //Does the previous things
      chr=serial->read();
      str+=chr;
    }
  }
  v=str.toInt();
  v+=180;
}

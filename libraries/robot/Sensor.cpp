#include <Sensor.h>
#include "Arduino.h"
#include <Wire.h>
#include <string.h>
#define TCAADDR 0x70


void sensor::init(){
  //Serial.println("Inizializzazione");

  sensor::SelectToF("FrontUp");
  delay(10);
  VL53L0X *temp=new VL53L0X;
  temp->init();
  temp->setTimeout(500);
  temp->setSignalRateLimit(0.1);
  temp->setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  temp->setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
  delete temp;
  sensor::SelectToF("FrontMid");
  delay(10);
  VL53L0X *temp1=new VL53L0X;
  temp1->init();
  temp1->setTimeout(500);
  temp1->setSignalRateLimit(0.1);
  temp1->setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  temp1->setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
  delete temp1;
  sensor::SelectToF("FrontDown");
  delay(10);
  VL53L0X *temp2=new VL53L0X;
  temp2->init();
  temp2->setTimeout(500);
  temp2->setSignalRateLimit(0.1);
  temp2->setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  temp2->setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
  /*delete temp;
  sensor::SelectToF("RightUp");
  delay(1);
  VL53L0X *temp=new VL53L0X;
  temp->init();
  temp->setTimeout(500);
  temp->setSignalRateLimit(0.1);
  temp->setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  temp->setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
  delete temp;
  sensor::SelectToF("RightDown");
  delay(1);
  VL53L0X *temp=new VL53L0X;
  temp->init();
  temp->setTimeout(500);
  temp->setSignalRateLimit(0.1);
  temp->setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  temp->setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);*/
  this->ToF=temp2;
  //Serial.println("Fine");
}


void sensor::TcaSelect(uint8_t i) {

  if (i > 7) return;


  Wire.beginTransmission(TCAADDR);

  Wire.write(1 << i);

  Wire.endTransmission();

}





void sensor::SelectToF(String position){
  //Serial.println("SelectToF");

  if(position=="FrontDown"){

    sensor::TcaSelect(3);

  }

  else if(position=="FrontMid"){

    sensor::TcaSelect(2);

  }

  else if(position=="FrontUp"){

    sensor::TcaSelect(4);

  }

}



void sensor::SetToFDistanceReading(int distance,String position){
//sensor::SelectToF(position);

  if(distance<90){

    ToF->setSignalRateLimit(0.25);

    ToF->setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 14);

    ToF->setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 10);

  }

  else{

    ToF->setSignalRateLimit(0.1);

    ToF->setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);

    ToF->setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);

  }

}



int sensor::ReadToFmm(String position){

  sensor::SelectToF(position);

  int distance;

  int letture[2];

  inizioReadToFmm:;
  //Serial.println("Inizio lettura");
  for (int i=0;i<2;i++){

      letture[i]=ToF->readRangeSingleMillimeters();
      //Serial.println(letture[i]);
      delay(1);

  }

  if(!(letture[0]-letture[1]<precision && letture[1]-letture[0]<precision)){

    goto inizioReadToFmm;

  }

  return (letture[0]+letture[1])/2;

}

#ifndef ReadNANO_H
#define ReadNANO_H

#include "Arduino.h"
#include <SoftwareSerial.h>
class ReadNANO {
  private:
    SoftwareSerial *serial;
  public:
    ReadNANO(int rx,int tx);
    int readNANO(int &o,int &v);
};

#endif

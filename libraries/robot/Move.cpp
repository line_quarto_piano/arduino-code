#include <Move.h>
#include <ReadNANO.h>
#include "Arduino.h"


void Move::init(int pinA_1,int pinA_2,int pinB_1,int pinB_2,int maxspeed){
  _pinA_1=pinA_1;
  _pinA_2=pinA_2;
  _pinB_1=pinB_1;
  _pinB_2=pinB_2;
  _maxspeed=maxspeed;
  pinMode(_pinA_1,OUTPUT);
  pinMode(_pinA_2,OUTPUT);
  pinMode(_pinB_1,OUTPUT);
  pinMode(_pinB_2,OUTPUT);
}

void Move::setspeeds(int v1,int v2) {
  int v1_abs=abs(v1);
  int v2_abs=abs(v2);
  if(v1_abs>_maxspeed) v1_abs=_maxspeed;
  if(v2_abs>_maxspeed) v2_abs=_maxspeed;
  if(v2<0){
    analogWrite(_pinA_1,v2_abs/*/divSPM0*/);
    analogWrite(_pinA_2,0);
  }
  else{
    analogWrite(_pinA_1,0);
    analogWrite(_pinA_2,v2_abs/*/divSPM0*/);
  }

  if(v1>0){
    analogWrite(_pinB_1,0);
    analogWrite(_pinB_2,v1_abs);
  }
  else{
    analogWrite(_pinB_1,v1_abs);
    analogWrite(_pinB_2,0);
  }
  Serial.print(v1_abs);
  Serial.print(" ");
  Serial.println(v2_abs);
}

void Move::sprint(){
  Move::setspeeds(_maxspeed,_maxspeed);
}

void Move::stop(){
  Move::setspeeds(0,0);
}

void Move::clock(int v){    //20
  Move::setspeeds(v,-v);
}

void Move::counterclock(int v){
  Move::setspeeds(-v,v);
}

int Move::rotateFor(int angle) {
  ReadNANO imuReading(0,0);
  int x,y,angleTo;
  imuReading.readNANO(x,y);
  angleTo=x+angle;
  if(angleTo>360) {
    angleTo-=360;
  }
  else if(angleTo<0){
    angleTo+=360;
  }

  if(angle>0)           //0:CCW; 1:CW
    rotateDir=1;
  else
    rotateDir=0;

  //Serial.println("Begin rotation");

  return angleTo;
}

void Move::rotateTo(int angle, int tolerance, int speed){
  ReadNANO imuReading(0,0);
  int x,y,rotazione;
  imuReading.readNANO(x,y);

  begin_rotateto:;
  if(rotateDir){
    Move::clock(speed);}
  else{
    Move::counterclock(speed);}

  /*
  Serial.print("Angle where I am: ");
  Serial.println(x);
  Serial.print("Angle to go: ");
  Serial.println(angle);
  Serial.println("Rotating");
  */

  while(x<angle-tolerance || x>angle+tolerance) {
    imuReading.readNANO(x,y);
  }
  Move::stop();
  delay(100);
  if(x<angle-tolerance || x>angle+tolerance){
    rotateDir=!rotateDir;
    goto begin_rotateto;
  }
  //Serial.println("Finished the rotation");
}

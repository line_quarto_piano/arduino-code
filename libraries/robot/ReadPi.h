#ifndef ReadPi_H
#define ReadPi_H

#include "Arduino.h"

class ReadPi {
  public:
    int readPi(int &o,int &v);
};

#endif

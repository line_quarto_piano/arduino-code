#include <lastRoom.h>
#include <Sensor.h>
#include <Move.h>
#include <ReadNano.h>
#include <ReadPi.h>

#define speed 50
#define distance 50

void LastRoom::init(int pinA_1,int pinA_2,int pinB_1,int pinB_2,int maxspeed){
  LastRoom::Movements.init(int pinA_1,int pinA_2,int pinB_1,int pinB_2,int maxspeed)
}

bool LastRoom::Compare(int tolerance,String side){
  int Up=LastRoom::Sensors.ReadToFmm(side+"Up");
  int Down=LastRoom::Sensors.ReadToFmm(side+"Down");
  if(Up>Down+tolerance)
    return true;
  else
    return false;
}

void LastRoom::TakeBall(){
  int dist,rot;
  do{
    LastRoom::Camera.readPi(dist,rot);
    LastRoom::Movements.rotateTo(LastRoom::Movements.rotateFor(rot), 2, speed);
  }while(abs(rot)>1);
  LastRoom::Movements.setspeeds(speed,speed);
  while(LastRoom::Sensors.ReadToFmm("Front")>distance){}
  /*Abassa l'asta*/
  LastRoom::Movements.stop();
  LastRoom::Movements.setspeeds(-speed,-speed);
  delay(100);
  LastRoom::Movements.stop();
  LastRoom::Camera.readPi(dist,rot)
  if(dist!=0 && abs(rot)<(Distance/9))
    LastRoom::TakeBall();
  return;
}

void LastRoom::ReturnHome(bool pos){
  LastRoom::Movements.rotateTo(LastRoom::Movements.rotateFor(180), 2, speed);
  LastRoom::Movements.setspeeds(speed,speed);
  while(LastRoom::Sensors.ReadToFmm("Front")>450){}
  LastRoom::Movements.setspeeds(-speed,-speed);
  while(LastRoom::Sensors.ReadToFmm("Front")<450){}
  LastRoom::Movements.stop();
  LastRoom::Movements.rotateDir=pos;
  LastRoom::Movements.rotateTo(0, 2, speed);
  LastRoom::Movements.setspeeds(-speed,-speed);
  delay(500);
  LastRoom::Movements.stop();
}

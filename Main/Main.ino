
/*
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                                                  LINE FOLLOWER 2016-17 LICEO SCIENTIFICO GALILEO GALILEI
                                                              Gruppo SLACK: QuelliDelQuartoPiano
                                                              Gruppo BIT_BUCKET: LINE_quarto_piano
                                                              Gruppo HANGOUTS: tech addicts anonymous

                                                              PARTECIPANTI:
                                                              -Debortoli Francesco 3Asa
                                                              -Chippendale Felix 3Asa
                                                              -Leonardelli Paolo 3Asa
                                                              -Lazzeri Elia 3Asa                  
                                                              -Girardi Alessandro 3Asa
                                                              -Nogler Jakob 3Aord
                                                          
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/




#include <Wire.h>
//#include "line_library.h"
#include <ReadNANO.h>
#include <Move.h>
//#include <Sensor.h>
//#include <ReadPi.h>
//#include <lastRoom.h>

#define rightMotor1 10
#define rightMotor2 11
#define leftMotor1 8
#define leftMotor2 9

int maxspeed=255,basespeed=30;


/*
 * Variabili Globali
 * 
*/
int line_data[16], idx; 
int rightpower, leftpower;

int norm_speed = 50;
double proportional = 0, integral = 0, derivative = 0, last_proportional = 0, error_value = 0;
int Kp = 10, Ki = 4, Kd = 0; //valori dei K da trovare sperimentalmente
double pos_linea = 0, somma = 0;

Move movements;
//sensor Sensors;

/*********************************************************************************************************************/




void obstacle() {
  movements.stop();
  movements.rotateTo(movements.rotateFor(-30), 3, basespeed);
  movements.setspeeds(basespeed, basespeed);
  delay(750);
  movements.stop();
  movements.rotateTo(movements.rotateFor(30), 3, basespeed);
  movements.setspeeds(basespeed, basespeed);
  delay(600);
  movements.rotateTo(movements.rotateFor(90), 3, basespeed);
  movements.setspeeds(basespeed, basespeed);
  delay(100);
  /*line.ReadSensors(times);
  line.FindError(times, rap, &cases);

  while (cases == 1) {

    line.ReadSensors(times);

    line.FindError(times, rap, &cases);

  }*/

  movements.rotateTo(movements.rotateFor(-90), 3, basespeed);

  movements.setspeeds(-basespeed, -basespeed);

  delay(1000);

}







void setup(){
//  Sensors.init();
  Wire.begin();
  idx = 0;

  movements.init(leftMotor1,leftMotor2,rightMotor1,rightMotor2,maxspeed);
  
  movements.stop(); 
  Serial.begin(9600);
}



void loop(){

  /*if(Ho visto ostacolo)
    obstacle();
*/
  Wire.requestFrom( 9, 16);
  while(Wire.available())
  { 
    line_data[idx] = Wire.read();
    if(idx < 15)
      idx++;
    else
      idx = 0;
  }

  //conversione dei dati linea in distanza dalla sinistra del sensore
  int data[8], minimo = 300, massimo = 0, cont = 0;
  for(int i = 0; i < 8; i++){
    data[i] = line_data[i*2];
    if(data[i] < minimo)
      minimo = data[i];
    if(data[i] > massimo)
      massimo = data[i];
  }
  
  int range = massimo - minimo;
  for(int i = 0; i < 8; i++){
    if(data[i] < minimo + (range / 3)){
      data[i] = 2;
      cont++;
    }
    else if(data[i] < minimo + 2 * (range / 3))
      data[i] = 1;
    else
      data[i] = 0;
  }
  
  pos_linea = 0, somma = 0;
  for(int i = 0; i < 8; i++)
  {
     somma += data[i];
     pos_linea += data[i] * i;
  }
  
  proportional = 0, derivative = 0;
  pos_linea = pos_linea / somma;
  if(cont == 0)
    pos_linea = 3.5;
  
  proportional = pos_linea - 3.5;
  if(proportional * last_proportional < 0)
    integral = 0;  
  
  integral = integral + proportional; 
  if(proportional < 0.5 && proportional > - 0.5)
      integral = 0;
  derivative = proportional - last_proportional;
  last_proportional = proportional;
  
  error_value = int(proportional * Kp + integral * Ki + derivative * Kd);
  
  //Serial.println(error_value);
  
  
  rightpower = norm_speed - error_value;
  leftpower = norm_speed + error_value;

  movements.setspeeds(leftpower,rightpower);

}



